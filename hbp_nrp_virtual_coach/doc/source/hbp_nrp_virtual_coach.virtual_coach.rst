Virtual Coach
=============

.. automodule:: hbp_nrp_virtual_coach
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`VirtualCoach` Module
--------------------------

.. automodule:: hbp_nrp_virtual_coach.virtual_coach
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Simulation` Module
------------------------

.. automodule:: hbp_nrp_virtual_coach.simulation
    :members:
    :undoc-members:
    :show-inheritance:
