Introduction
============

The Virtual Coach is a Python API that allows you to run and interact with experiments by scripting them instead of having to use the Web Cockpit. It is especially ideal for running learning experiments, where usually one experiment has to be run multiple times, each time with a different parameterization, and where intermediate results have to be saved to compare the effectiveness of different parameters at the end. Scripting experimetns does not restrict the viusalization. While an experiment is running from the Virtual Coach, you may still open the frontend and visualize what is happening in your experiments.

Users now can launch experiments from the Virtual Coach, interact with the simulation by adding, deleting or editing Transfer Functions and State Machines as well as modify the Brain Model and the Neural Populations on the fly. Also, CSV data that is being saved during a simulation can be accessed from the Virtual Coach and you can then plot the data using your own plotting functions for example. Additionally, you can reuse the same reset functionality found in the Web Cockpit from the Virtual Coach, meaning that after certain events have occured (a collision for example), or after running a simulation for a certain amount of time you can either reset the robot pose, the brain model, the environment or reset the whole simulation.

There is a necessary configuration file that must be copied from the user-scripts repository ($HBP/user-scripts/config_files/VirtualCoach/config.json) to the VirtualCoach repository in order for the Virtual Coach to run. You can either copy this file manually, or you can just run the configure_nrp script in the user-scripts to automatically copy it.

There is a special alias for running the Virtual Coach. For this alias to work, $HBP/user-scripts/nrp_aliases has to be sourced in your bash.rc. Again, the configure_nrp script takes care of that automatically. The Virtual Coach alias is `cle-virtual-coach` and can be run in three different ways:

1. **Launch a Jupyter Notebook session**

   To launch a Jupyter Notebook session just run `cle-virtual-coach jupyter notebook` in a terminal. Of course, Jupyter Notebook has to be installed prior.

2. **Launch an interactive python interpreter session**

   To launch an interactive python interpreter session just run `cle-virtual-coach python` in a terminal.

3. **Launch a python script with arguments**

   To launch a python script `foo.py` with arguments `a b c` just run `cle-virtual-coach foo.py a b c`.

This information is also available in the alias help `cle-virtual-coach -h`.

In the next page you will find a desciption of the API, and if you have the local Virtual Coach repository you can check out the VirtualCoach/examples directory for some examples. Each example includes a README that may be useful to read before running it. Note that examples in the repository may be saved in a jupyter notebook as it makes it easier to run everything step-by-step and visualize results in place.

