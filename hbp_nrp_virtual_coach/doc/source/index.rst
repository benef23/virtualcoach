.. python_skeleton documentation master file, created by
   sphinx-quickstart on Wed Jan 29 14:44:00 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Virtual Coach documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 2
   
   introduction
   hbp_nrp_virtual_coach


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

