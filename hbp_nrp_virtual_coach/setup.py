'''setup.py'''

# pylint: disable=F0401,E0611,W0142

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import hbp_nrp_virtual_coach
import pip

from optparse import Option
options = Option('--workaround')
options.skip_requirements_regex = None
reqs_file = './requirements.txt'
# Hack for old pip versions
if pip.__version__.startswith('10.'):
    # Versions greater or equal to 10.x don't rely on pip.req.parse_requirements
    install_reqs = list(val.strip() for val in open(reqs_file))
    reqs = install_reqs
elif pip.__version__.startswith('1.'):
    # Versions 1.x rely on pip.req.parse_requirements
    # but don't require a "session" parameter
    from pip.req import parse_requirements # pylint:disable=no-name-in-module, import-error
    install_reqs = parse_requirements(reqs_file, options=options)
    reqs = [str(ir.req) for ir in install_reqs]
else:
    # Versions greater than 1.x but smaller than 10.x rely on pip.req.parse_requirements
    # and requires a "session" parameter
    from pip.req import parse_requirements # pylint:disable=no-name-in-module, import-error
    from pip.download import PipSession  # pylint:disable=no-name-in-module, import-error
    options.isolated_mode = False
    install_reqs = parse_requirements(  # pylint:disable=unexpected-keyword-arg
        reqs_file,
        session=PipSession,
        options=options
    )
    reqs = [str(ir.req) for ir in install_reqs]

config = {
    'description': 'Virtual Coach command-line Neurorobotics Platform interface for HBP SP10',
    'author': 'HBP Neurorobotics',
    'url': 'http://neurorobotics.net',
    'author_email': 'neurorobotics@humanbrainproject.eu',
    'version': hbp_nrp_virtual_coach.__version__,
    'install_requires': reqs,
    'packages': ['hbp_nrp_virtual_coach'],
    'package_data': {
        'hbp_nrp_virtual_coach': ['config.json']
    },
    'scripts': [],
    'name': 'hbp-nrp-virtual-coach',
    'include_package_data': True,
}

setup(**config)
