"""
This package contains the Virtual Coach implementation and interfaces to the Neurorobotics
Platform backend services.
"""

from hbp_nrp_virtual_coach.version import VERSION as __version__  # pylint: disable=W0611
